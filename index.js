const PORT = process.env.PORT || 8000;

const express = require('express');
const axios = require('axios');
const cheerio = require('cheerio');

const app = express();

const news = [
    {
        name: 'thetimes',
        adress: 'https://www.thetimes.co.uk/environment/climate-change',
        base: ''
    },
    {
        name: 'theguardian',
        adress: 'https://www.theguardian.com/environment/climate-crisis',
        base: ''
    },
    {
        name: "telegraph",
        adress: 'https://www.telegraph.co.uk/climate-change/',
        base: 'https://www.telegraph.co.uk/'
    }
];

const articles = [];

news.forEach(news => {
    axios.get(news.adress).then((response) => {
        const html = response.data;
        const $ = cheerio.load(html);

        $('a:contains("climate")', html).each(function () {
            const title = $(this).text();
            const url = $(this).attr('href');
            articles.push({
                title,
                url: news.base + url,
                source: news.name
            });
        });
    })
});

app.get('/', function (req, res) {
    res.json('Server lisening')
});

app.get('/news', function (req, res) {
    res.json(articles);
});

app.get('/news/:newsId', (req, res) => {
    const newsId = req.params.newsId;

    const newsUrl = news.filter(news => news.name == newsId)[0].adress;
    const newsUrlBase = news.filter(news => news.name == newsId)[0].base;

    axios.get(newsUrl).then(function (response) {
        const html = response.data;
        const $ = cheerio.load(html);
        const specificArticles = [];

        $('a:contains("climate")', html).each(function () {
            const title = $(this).text();
            const url = $(this).attr('href');

            specificArticles.push({
                title,
                url: newsUrlBase + url,
                source: newsId
            });
        });

        res.json(specificArticles);
    }).catch(err => console.log(err));
});

app.listen(PORT, () => console.log(`server running on port: ${PORT}`));
